import pygtk
pygtk.require('2.0')
import gtk

import sys
import apt
import string
import urllib2
import re
import apt_pkg
import gettext

_ = gettext.gettext

def str_to_bool(str):
  if str == "0" or str.upper() == "FALSE":
    return False
  return True

def utf8(str):
  return unicode(str, 'latin1').encode('utf-8')

class GtkProgress(apt.OpProgress):
  def __init__(self, progressbar):
    self._progressbar = progressbar
  def Update(self, percent):
    self._progressbar.show()
    self._progressbar.set_text(self.Op)
    self._progressbar.set_fraction(percent/100.0)
    while gtk.events_pending():
      gtk.main_iteration()
  def Done(self):
    self._progressbar.hide()


CHANGELOGS_URI = None
sys.path.insert (0, "/usr/share/update-manager/python/")
from DistInfo import DistInfo
dinfo = DistInfo ()
CHANGELOGS_URI = dinfo.changelogs_uri
del DistInfo, dinfo
if not CHANGELOGS_URI:
  CHANGELOGS_URI="http://changelogs.ubuntu.com/changelogs/pool/%s/%s/%s/%s_%s/changelog"

def get_changelog(pkg, lock, all_changes):
    srcpkg = pkg.sourcePackageName

    src_section = "main"
    l = string.split(pkg.section,"/")
    if len(l) > 1:
      sec_section = l[0]
    
    prefix = srcpkg[0]
    if srcpkg.startswith("lib"):
      prefix = "lib" + srcpkg[3]

    verstr = pkg.candidateVersion
    l = string.split(verstr,":")
    if len(l) > 1:
      verstr = l[1]

    try:
      uri = CHANGELOGS_URI % (src_section,prefix,srcpkg,srcpkg, verstr)
      changelog = urllib2.urlopen(uri)
      #print changelog.read()
      # do only get the lines that are new
      alllines = ""
      regexp = "^%s \((.*)\)(.*)$" % (srcpkg)

      i=0
      while True:
        line = changelog.readline()
        #print line
        if line == "":
          break
        match = re.match(regexp,line)
        if match:
          if apt_pkg.VersionCompare(match.group(1),pkg.installedVersion) <= 0:
            break
          # EOF (shouldn't really happen)
        alllines = alllines + line

      # only write if we where not canceld
      if lock.locked():
        all_changes[pkg.name] = [alllines, srcpkg]
    except urllib2.HTTPError:
      if lock.locked():
        all_changes[pkg.name] = [_("Changes not found, the server may not be updated yet."), srcpkg]
    except IOError:
      if lock.locked():
        self.all_changes[pkg.name] = [_("Failed to download changes. Please check if there is an active internet connection."), srcpkg]

    if lock.locked():
      lock.release()
