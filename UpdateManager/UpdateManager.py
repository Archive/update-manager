#!/usr/bin/env python2.4
# UpdateManager - easy updating application
#  
#  Copyright (c) 2004,2005 Canonical
#                2004 Michiel Sikkes
#  
#  Author: Michiel Sikkes <michiel@eyesopened.nl>
#          Michael Vogt <mvo@debian.org>
# 
#  This program is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU General Public License as 
#  published by the Free Software Foundation; either version 2 of the
#  License, or (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
#  USA

import pygtk
pygtk.require('2.0')
import gtk
import gtk.gdk
import gtk.glade
import gobject
import gnome
import gettext
import copy
import string
import sys
import os
import os.path
import urllib2
import re
import thread
import tempfile
import time
import rfc822
import gconf
import pango
import subprocess
import apt
import apt_pkg

from Utils import utf8, str_to_bool, GtkProgress, get_changelog
from GladeApp import GladeApp
from UpdateView import UpdateTreeModel, LIST_ACTION, LIST_CONTENTS, LIST_PKG, LIST_NAME


_ = gettext.gettext

# FIXME: port the "version details" patch


# actions for "invoke_manager"
(INSTALL, UPDATE) = range(2)

SYNAPTIC_PINFILE = "/var/lib/synaptic/preferences"
SYNAPTIC_CONF_FILE = "/root/.synaptic/synaptic.conf"

#METARELEASE_URI = "http://people.ubuntu.com/~mvo/meta-release-test"
METARELEASE_URI = None
sys.path.insert (0, "/usr/share/update-manager/python/")
from DistInfo import DistInfo
dinfo = DistInfo ()
METARELEASE_URI = dinfo.metarelease_uri
del DistInfo, dinfo
if not METARELEASE_URI:
  METARELEASE_URI="http://changelogs.ubuntu.com/meta-release"
METARELEASE_FILE = "/var/lib/update-manager/meta-release"


class UpgradesFilter(apt.cache.Filter):
    """ a filter that returns everything that is upgradable """
    def apply(self, pkg):
        if pkg.markedInstall or pkg.markedDelete or \
           pkg.markedUpgrade or pkg.isUpgradable:
            return True
        else:
            return False
    
class UpdateManager(GladeApp):
  def __init__(self, datadir):
    GladeApp.__init__(self, datadir=datadir)
    self.gnome_program = gnome.init("update-manager", "0.41")
    self.packages = []
    self.dl_size = 0
    self.all_changes = {}
    self.dist = self.get_dist()

    self.StatusText = self._glade.get_widget("status_text")
    self.DescView = self._glade.get_widget("descview")
    self.ChangesView = self._glade.get_widget("textview_changes")
    changes_buffer = self.ChangesView.get_buffer()
    changes_buffer.create_tag("versiontag", weight=pango.WEIGHT_BOLD)
    self.expander = self._glade.get_widget("expander_details")
   
    self.installbutton = self._glade.get_widget("button_install")
    self.gconfclient = gconf.client_get_default()
    # restore state
    self.restore_state()

    # proxy stuff
    if os.path.exists(SYNAPTIC_CONF_FILE):
      cnf = apt_pkg.newConfiguration()
      apt_pkg.ReadConfigFile(cnf, SYNAPTIC_CONF_FILE)
      use_proxy = cnf.FindB("Synaptic::useProxy", False)
      if use_proxy:
        proxy_host = cnf.Find("Synaptic::httpProxy")
        proxy_port = str(cnf.FindI("Synaptic::httpProxyPort"))
        if proxy_host and proxy_port:
          proxy_support = urllib2.ProxyHandler({"http":"http://%s:%s" % (proxy_host, proxy_port)})
          opener = urllib2.build_opener(proxy_support)
          urllib2.install_opener(opener)

    # init 
    self.initCache()
    self.cache.upgrade(True)
    self.init_tree_view()
    self.expander.connect("notify::expanded", self.activate_details)
    
    self.check_updates()
    
  def check_updates(self):
    count = len(self.cache.keys())
    if (count == 0):
      self.set_status(_("Your system is up-to-date!"))
      return False
    else:
      if (count == 1):
        self.set_status(_("There is one package available for updating."))
      else:
        self.set_status(_("There are %s packages available for updating.") % count)
      return True


  def update_row_activated(self, treeview, path, view_column):
    #print "activated: %s %s" % (path, view_column)
    model = treeview.get_model()
    pkg = model[path][LIST_PKG]
    if pkg.markedInstall or pkg.markedUpgrade:
        pkg.markKeep()
    else:
        pkg.markInstall()

  def init_tree_view(self):
    self.treeview = self._glade.get_widget("updatelist")
    self.treeview.set_model(UpdateTreeModel(self.cache))
    self.treeview.connect("row-activated", self.update_row_activated)
    self.treeview.connect('cursor-changed', self.cursor_changed)

    tr = gtk.CellRendererText()
    tr.set_property("xpad", 10)
    tr.set_property("ypad", 10)
    cr = gtk.CellRendererPixbuf()
    cr.set_property("xpad", 10)
    self.cb = gtk.TreeViewColumn("Action", cr, pixbuf=LIST_ACTION)
    c0 = gtk.TreeViewColumn("Name", tr, markup=LIST_CONTENTS)
    c0.set_resizable(True)
    major,minor,patch = gtk.pygtk_version
    if (major >= 2) and (minor >= 5):
      self.cb.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
      self.cb.set_fixed_width(30)
      c0.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
      c0.set_fixed_width(100)
      self.treeview.set_fixed_height_mode(True)

    self.treeview.append_column(self.cb)
    #self.cb.set_visible(False);
    self.treeview.append_column(c0)
    self.treeview.set_search_column(LIST_NAME)
    #self.treeview.append_column(c1)
    #self.treeview.append_column(c2)
    #self.treeview.set_headers_visible(False)
    # set expander to last position


  def set_changes_buffer(self, changes_buffer, text, name, srcpkg):
    changes_buffer.set_text("")
    lines = text.split("\n")
    if len(lines) == 1:
      changes_buffer.set_text(text)
      return
    
    for line in lines:
    
      end_iter = changes_buffer.get_end_iter()
      
      version_match = re.match("^%s \((.*)\)(.*)$" % (srcpkg), line)
      #bullet_match = re.match("^.*[\*-]", line)
      author_match = re.match("^.*--.*<.*@.*>.*$", line)
      if version_match:
        version = version_match.group(1)
        version_text = _("Version %s: \n") % version
        changes_buffer.insert_with_tags_by_name(end_iter, version_text, "versiontag")
      # mvo: disabled for now as it does not catch multi line entries
      #      (see ubuntu #7034 for rational)
      #elif bullet_match and not author_match:
      #  bullet_text = "    " + line + "\n"
      #  changes_buffer.insert(end_iter, bullet_text)
      elif (author_match):
        pass
        #chanages_buffer.insert(end_iter, "\n")
      else:
        changes_buffer.insert(end_iter, line+"\n")
        

  def cursor_changed(self, treeview):
    #print "cursor_changed()"
    (path, column) = treeview.get_cursor()
    # check if we have a path at all
    if path == None:
        return
  
    model = treeview.get_model()
    iter = model.get_iter(path)

    # set descr
    pkg = model.get_value(iter, LIST_PKG)
    long_desc = pkg.description
    if long_desc == None:
      return
    # remove the first line
    #s = "\n".join([line for line in string.split(long_desc,"\n")][1:])
    s = long_desc[long_desc.find("\n")+1:]
    desc_buffer = self.DescView.get_buffer()
    desc_buffer.set_text(utf8(s))

    # now do the changelog
    name = pkg.name
    if name == None:
      return

    changes_buffer = self.ChangesView.get_buffer()
    
    # check if we have the changes already
    if self.all_changes.has_key(name):
      changes = self.all_changes[name]
      self.set_changes_buffer(changes_buffer, changes[0], name, changes[1])
    else:
      if self.expander.get_expanded():
        self.treeview.set_sensitive(False)
        self._glade.get_widget("hbox_footer").set_sensitive(False)
        lock = thread.allocate_lock()
        lock.acquire()
        #get_changelog(pkg, lock, self.all_changes)
        t=thread.start_new_thread(get_changelog,
                                  (pkg, lock, self.all_changes))
        changes_buffer.set_text(_("Downloading changes..."))
        button = self._glade.get_widget("button_cancel_dl_changelog")
        button.show()
        id = button.connect("clicked",
                            lambda w,lock: lock.release(), lock)
        # wait for the dl-thread
        while lock.locked():
          time.sleep(0.05)
          while gtk.events_pending():
            gtk.main_iteration()
        # download finished (or canceld, or time-out)
        button.hide()
        button.disconnect(id);
        self.treeview.set_sensitive(True)
        self._glade.get_widget("hbox_footer").set_sensitive(True)
      
      if self.all_changes.has_key(name):
        changes = self.all_changes[name]
        self.set_changes_buffer(changes_buffer, changes[0], name, changes[1])

  def update_status(self):
    print "update_status"
    num_pkg = len(self.list.pkgs)
    num_selected_pkg = len(self.packages)
    if num_pkg == 0:
      text = _("There are no updated packages")
      self.StatusText.set_text (text)
    elif num_selected_pkg == 0:
      text = gettext.ngettext("You did not select any of the %s updated "
                              "package",
                              "You did not select any of the %s updated "
                              "packages", num_pkg) % ("<b>%i</b>" % (num_pkg))
      self.StatusText.set_markup (text)
    elif num_selected_pkg == num_pkg:
      text = gettext.ngettext("You have selected %s updated package, size %s",
                              "You have selected all %s updated packages, "
                              "total size %s", num_pkg) % ("<b>%i</b>" % num_selected_pkg, "<b>%s</b>" % apt_pkg.SizeToStr(self.dl_size))
      self.StatusText.set_markup (text)
    else:
      text = gettext.ngettext("You have selected %s out of %s updated "
                              "package, size %s",
                              "You have selected %s out of %s updated "
                              "packages, total size %s", num_pkg) % ("<b>%i</b>" % num_selected_pkg, "<b>%i</b>" % num_pkg, "<b>%s</b>" % apt_pkg.SizeToStr(self.dl_size))
      self.StatusText.set_markup (text)

  def package_list_changed(self):
    print "package_list_changed"
    self.update_status()
    if len(self.packages) == 0:
      self.installbutton.set_sensitive(False)
    else:
      self.installbutton.set_sensitive(True)

  def remove_update(self, pkg):
    name = pkg.name
    if name in self.packages:
      self.packages.remove(name)
      self.dl_size -= pkg.packageSize()
      self.package_list_changed()

  def add_update(self, pkg):
    name = pkg.name
    if name not in self.packages:
      self.packages.append(name)
      self.dl_size += pkg.packageSize()
      self.package_list_changed()

  def activate_details(self, expander, data):
    expanded = self.expander.get_expanded()
    self.gconfclient.set_bool("/apps/update-manager/show_details",expanded)
    if expanded:
      self.cursor_changed(self.treeview)

  def run_synaptic(self, id, action, lock):
    cmd = ["/usr/sbin/synaptic",
           "-o"
           "Volatile::SetSelectionsNoFix=true",
           "--hide-main-window",
           "--non-interactive",
           "--plug-progress-into", "%s" % (id) ]
    if action == INSTALL:
      cmd.append("--set-selections")
      cmd.append("--progress-str")
      cmd.append("%s" % _("The updates are being applied."))
      cmd.append("--finish-str")
      cmd.append("%s" %  _("Upgrade finished"))
      proc = subprocess.Popen(cmd, stdin=subprocess.PIPE)
      f = proc.stdin
      #for s in self.packages:
      #  f.write("%s\tinstall\n" % s)
      for pkg in self.cache.getChanges():
          if pkg.markedUpgrade or pkg.markedInstall:
              f.write("%s\tinstall\n" % pkg.name)
          elif pkg.markedDelete:
              print "REMOVING: %s " % pkg.name
              f.write("%s\tdeinstall\n" % pkg.name)
          else:
              print "don't know what to do with: %s " % pkg.name
      f.close()
      proc.wait()
    elif action == UPDATE:
      cmd.append("--update-at-startup")
      subprocess.call(cmd)
    else:
      print "run_synaptic() called with unknown action"
      sys.exit(1)

    # use this once gksudo does propper reporting
    #if os.geteuid() != 0:
    #  if os.system("gksudo  /bin/true") != 0:
    #    return
    #  cmd = "sudo " + cmd;
    lock.release()

  def plug_removed(self, w, (win,socket)):
    #print "plug_removed"
    # plug was removed, but we don't want to get it removed, only hiden
    # unti we get more 
    win.hide()
    return True

  def plug_added(self, sock, win):
    win.show()
    while gtk.events_pending():
      gtk.main_iteration()

  # callbacks
  def on_window_UpdateManager_delete_event(self, widget, event):
    print "on_window_UpdateManager_delete_event()"
    self.exit()

  def on_button_close_clicked(self, widget):
    self.exit()

  def on_button_reload_clicked(self, widget):
    #print "on_button_reload_clicked"
    self.invoke_manager(UPDATE)

  def on_button_help_clicked(self, widget):
    gnome.help_display_desktop(self.gnome_program, "update-manager",
                               "update-manager", "")

  def on_button_install_clicked(self, widget):
    print "on_button_install_clicked"
    self.invoke_manager(INSTALL)

  def invoke_manager(self, action):
    # check first if no other package manager is runing
    import struct, fcntl
    lock = os.path.dirname(apt_pkg.Config.Find("Dir::State::status"))+"/lock"
    lock_file= open(lock)
    flk=struct.pack('hhllhl',fcntl.F_WRLCK,0,0,0,0,0)
    try:
      rv = fcntl.fcntl(lock_file, fcntl.F_GETLK, flk)
    except IOError:
      print "Error getting lockstatus"
      raise
    locked = struct.unpack('hhllhl', rv)[0]
    if locked != fcntl.F_UNLCK:
      msg=("<big><b>%s</b></big>\n\n%s"%(_("Another package manager is running"),
                                         _("You can run only one "
                                           "package management application "
                                           "at the same time. Please close "
                                           "this other application first.")));
      dialog = gtk.MessageDialog(None, 0, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,"")
      dialog.set_default_response(gtk.RESPONSE_OK)
      dialog.set_markup(msg)
      dialog.run()
      dialog.destroy()
      return

    # don't display apt-listchanges, we already showed the changelog
    os.environ["APT_LISTCHANGES_FRONTEND"]="none"

    # set window to insensitive
    self._win.set_sensitive(False)
    # create a progress window that will swallow the synaptic progress bars
    win = gtk.Window()
    if action==UPDATE:
      msg = _("Updating package list...")
      win.set_title(msg)
      self.set_status(_("Checking for updates..."))
    else:
      msg = _("Installing updates...")
      win.set_title(msg)
      self.set_status(msg)
    win.set_border_width(6)
    win.set_transient_for(self._win)
    win.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    win.resize(400,200)
    win.set_resizable(False)
    # prevent the window from closing with the delete button (there is
    # a cancel button in the window)
    win.connect("delete_event", lambda e,w: True);
    
    # create the socket
    socket = gtk.Socket()
    socket.show()
    win.add(socket)

    socket.connect("plug-added", self.plug_added, win)
    socket.connect("plug-removed", self.plug_removed, (win,socket))
    lock = thread.allocate_lock()
    lock.acquire()
    t = thread.start_new_thread(self.run_synaptic,(socket.get_id(),action,lock))
    while lock.locked():
      while gtk.events_pending():
        gtk.main_iteration()
      time.sleep(0.05)
    win.destroy()
    while gtk.events_pending():
      gtk.main_iteration()
    #self.fillstore()
    self.set_status("Generating list of updates...")
    self.openCache(self.progress)
    #self.initCache()
    self._win.set_sensitive(True)    
    
    self.check_updates()

#   def toggled(self, renderer, path_string):
#     """ a toggle button in the listview was toggled """
#     iter = self.store.get_iter_from_string(path_string)
#     if self.store.get_value(iter, LIST_INSTALL):
#       self.store.set_value(iter, LIST_INSTALL, False)
#       self.remove_update(self.store.get_value(iter, LIST_PKG))
#     else:
#       self.store.set_value(iter, LIST_INSTALL, True)
#       self.add_update(self.store.get_value(iter, LIST_PKG))

  def set_status(self, text):
    """ set status text """
    widget = self._glade.get_widget("status_text")
    widget.set_markup(text)

  def exit(self):
    """ exit the application, save the state """
    self.save_state()
    gtk.main_quit()

  def save_state(self):
    """ save the state  (window-size for now) """
    (x,y) = self._win.get_size()
    self.gconfclient.set_pair("/apps/update-manager/window_size",
                              gconf.VALUE_INT, gconf.VALUE_INT, x, y)

  def restore_state(self):
    """ restore the state (window-size for now) """
    expanded = self.gconfclient.get_bool("/apps/update-manager/show_details")
    self.expander.set_expanded(expanded)
    (x,y) = self.gconfclient.get_pair("/apps/update-manager/window_size",
                                      gconf.VALUE_INT, gconf.VALUE_INT)
    if x > 0 and y > 0:
      self._win.resize(x,y)

  def on_button_preferences_clicked(self, widget):
    """ start gnome-software preferences """
    # args: "-n" means we take care of the reloading of the
    # package list ourself
    args = ['/usr/bin/gnome-software-properties', '-n']
    child = subprocess.Popen(args)
    self._win.set_sensitive(False)
    res = None
    while res == None:
      res = child.poll()
      time.sleep(0.05)
      while gtk.events_pending():
        gtk.main_iteration()
    # repository information changed, call "reload"
    if res > 0:
      self.on_button_reload_clicked(None)
    self._win.set_sensitive(True)



  # FIXME: portme, never called anymore
  def fillstore(self):
    print "fillstore()"

    # clean most objects
    self.packages = []
    self.dl_size = 0
    self.all_changes = {}
    self.store.clear()
    self.openCache()
    self.list = UpdateList()

    # fill them again
    self.list.update(self.cache)
    if self.list.num_updates < 1:
      # set the label and treeview and hide the checkbox column
      self.cb.set_visible(False)
      self.treeview.hide()
      self.expander.hide()
      label = self._glade.get_widget("label_header")
      text = "<big><b>%s</b></big>\n\n%s" % (_("Your system is up-to-date!"),
                                             _("There are no updates available."))
      label.set_markup(text)
      # make sure no install is possible
      self.installbutton.set_sensitive(False)
    else:
      self.cb.set_visible(True)
      self.treeview.show()
      self.expander.show()
      self.treeview.set_headers_visible(False)
      label = self._glade.get_widget("label_header")
      text = _("<big><b>Available Updates</b></big>\n"
               "\n"
               "The following packages are found to be upgradable. You can upgrade them by "
               "using the Install button.")
      label.set_markup(text)
      i=0
      for pkg in self.list.pkgs:

        # build version string
        show_version_details = self.gconfclient.get_bool("/apps/gnome-software-properties/show_version_details")
        if show_version_details:
          version = PackageVersion(pkg.name, pkg.installedVersion())
          contents = "<big><b>%s</b></big>\n<small>%s</small>\n<small><b>%s</b>\n%s</small>" % (pkg.name, pkg.summary, _("New version:"), version.get_version())
        else:
          contents = "<big><b>%s</b></big>\n<small>%s</small>" % (pkg.name, pkg.summary)
        iter = self.store.append([True, contents, pkg.name, pkg.summary, pkg.installedVersion(), pkg.description, pkg])
        self.add_update(pkg)
        i = i + 1


    self.update_status()
    return False

  def get_dist(self):
    try:
      return self.dist_codename
    except AttributeError:
      proc = subprocess.Popen(['/bin/lsb_release', '-c'], stdout=subprocess.PIPE)
      line = proc.stdout.readline()
      proc.stdout.close ()
      proc.wait ()
      key, value = line.split(":")
      if (key == "Codename"):
        self.dist_codename = value.strip ()
        return self.dist_codename

  def current_dist_not_supported(self, name):
    #print name
    msg = "<big><b>%s</b></big>\n\n%s" % (_("Your distribution is no longer supported"), _("Please upgrade to a newer version of Ubuntu Linux. The version you are running will no longer get security fixes or other critical updates. Please see http://www.ubuntulinux.org for upgrade information."))
    dialog = gtk.MessageDialog(None, 0, gtk.MESSAGE_WARNING, gtk.BUTTONS_OK,"")
    dialog.set_default_response(gtk.RESPONSE_OK)
    dialog.set_markup(msg)
    dialog.run()
    dialog.destroy()
    

  def new_dist_available(self, name):
    #print name
    # check if the user already knowns about this dist
    seen = self.gconfclient.get_string("/apps/update-manager/seen_dist")
    if name == seen:
      return
    
    msg = "<big><b>%s</b></big>\n\n%s" % (_("There is a new release of Ubuntu available!"), _("A new release with the codename '%s' is available. Please see http://www.ubuntulinux.org/ for upgrade instructions.") % name)
    dialog = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO,gtk.BUTTONS_CLOSE, "")
    dialog.set_default_response(gtk.RESPONSE_OK)
    dialog.set_markup(msg)
    check = gtk.CheckButton(_("Never show this message again"))
    check.show()
    dialog.vbox.pack_start(check)
    dialog.run()
    if check.get_active():
      self.gconfclient.set_string("/apps/update-manager/seen_dist",name)
    dialog.destroy()
    
  # code that does the meta release file checking
  def check_meta_release(self):
    #print "check_meta_release" 
    current_dist = self.dist
    dists = {}
    if self.metarelease_information != None:
      #print "meta_release found (current_dist: %s)" % (current_dist)
      # we have a meta-release file
      current_dist_date = 0
      current_dist_supported = False
      new_dist_available = False
      # parse it
      index_tag = apt_pkg.ParseTagFile(self.metarelease_information)
      step_result = index_tag.Step()
      while step_result:
        if index_tag.Section.has_key("Dist"):
          dist = index_tag.Section["Dist"]
          date = time.mktime(rfc822.parsedate(index_tag.Section["Date"]))
          dists[dist] = date
          if dist == current_dist:
            current_dist_supported = str_to_bool(index_tag.Section["Supported"])
            current_dist_date = time.mktime(rfc822.parsedate(index_tag.Section["Date"]))
        step_result = index_tag.Step()
      # check for newer dists
      new_dist = ""
      found = False
      for dist in dists:
        if dist == current_dist:
          found = True
        if dists[dist] > current_dist_date and not dist == current_dist:
          new_dist = dist
          current_dist_date = dists[dist]

      # we know nothing about the installed distro, so we just return
      # silently
      if not found:
        return False
      
      # only warn if unsupported and a new dist is available (because 
      # the development version is also unsupported)
      if new_dist != "" and not current_dist_supported:
        self.current_dist_not_supported(new_dist)
      elif new_dist != "":
        self.new_dist_available(new_dist)
      # don't run this event again
      return False
    # we have no information about the meta-release, so run it again
    return True

  # the network thread that tries to fetch the meta-index file
  def get_meta_release(self):
    lastmodified = 0
    req = urllib2.Request(METARELEASE_URI)
    if os.access(METARELEASE_FILE, os.W_OK):
      lastmodified = os.stat(METARELEASE_FILE).st_mtime
    if lastmodified > 0:
      req.add_header("If-Modified-Since", lastmodified)
    try:
      uri=urllib2.urlopen(req)
      f=open(METARELEASE_FILE,"w+")
      for line in uri.readlines():
        f.write(line)
      f.flush()
      f.seek(0,0)
      self.metarelease_information=f
      uri.close()
    except urllib2.URLError:
      pass

  # fixme: we should probably abstract away all the stuff from libapt
  def initCache(self):
    w = self._glade.get_widget("progressbar_cache")
    self.progress = progress = GtkProgress(w)
    self.real_cache = apt.cache.Cache()
    self.cache = apt.cache.FilteredCache(self.real_cache, progress=progress)

    # LOOKS LIKE WE HAVE A BUG HERE
    #self.cache._depcache.ReadPinFile()
    #if os.path.exists(SYNAPTIC_PINFILE):
    #  self.cache._depcache.ReadPinFile(SYNAPTIC_PINFILE)
    #  self.cache._depcache.Init(progress)

    self.cache.setFilter(UpgradesFilter())
    # work around a bug in apt (no prog->Done() in pkgDepCache::init())
    self.progress.Done()

  def openCache(self, progress):
    self.treeview.set_model(None)
    self.cache.open(progress)
    if os.path.exists(SYNAPTIC_PINFILE):
        # FIXME: ReadPinFile() has to move into python-apt somehow
        self.cache.cachePreChange()
        self.cache._depcache.ReadPinFile(SYNAPTIC_PINFILE)
        self.cache._depcache.Init(progress)
        self.cache.cachePostChange()
    # work around a bug in apt (no prog->Done() in pkgDepCache::init())
    self.treeview.set_model(UpdateTreeModel(self.cache))
    self.progress.Done()

  def main(self):
    # FIXME: stat a check update thread 
    self.metarelease_information = None
    t=thread.start_new_thread(self.get_meta_release, ())
    gobject.timeout_add(1000, self.check_meta_release)
    #self.get_meta_release()
    
    while gtk.events_pending():
      gtk.main_iteration()

    #self.fillstore()
    gtk.main()


if __name__ == "__main__":
  pass

