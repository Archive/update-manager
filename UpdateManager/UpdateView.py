#!/usr/bin/env python

import apt

import pygtk; pygtk.require("2.0")
from gtk import gdk
import gtk
import gobject
import time
import copy

# FIXME: if we know that the view does not change (e.g. if it's
#        filtering only for packagenames
#        -> don't use snapshot/check_for_changes

# list constants
(LIST_ACTION, LIST_CONTENTS, LIST_NAME, LIST_PKG) = range(4)


class UpdateTreeModel(gtk.GenericTreeModel):
    column_types = (gtk.gdk.Pixbuf, str, str, gobject.TYPE_PYOBJECT)
    column_names = ['Pix', 'Name', 'Content', 'obj']
    
    def __init__(self, cache, datadir=""):
        gtk.GenericTreeModel.__init__(self)
        self._cache=cache
        self._cache.connect("cache_pre_open", self.cache_pre_open)
        self._cache.connect("cache_post_open", self.cache_post_open)
        self._cache.connect("cache_pre_change", self.cache_pre_change)
        self._cache.connect("cache_post_change", self.cache_post_change)
        self._icons = []
        p = "/usr/share/synaptic/pixmaps/package-"
        self._icons.append(gdk.pixbuf_new_from_file(p+"upgrade.png"))
        self._icons.append(gdk.pixbuf_new_from_file(p+"install.png"))
        self._icons.append(gdk.pixbuf_new_from_file(p+"remove.png"))
        self._icons.append(gdk.pixbuf_new_from_file(p+"installed-outdated.png"))
        self.update()

    def update(self):
        self._keys = self._cache.keys()
        self._keys.sort()
        return self._keys

    # the treeview stuff
    def on_get_flags(self):
        #print "on_get_flags()"
        return gtk.TREE_MODEL_LIST_ONLY

    def on_get_n_columns(self):
        #print "on_get_n_columns()"
        return len(self.column_types)

    def on_get_column_type(self, n):
        #print "on_get_column_type()"
        return self.column_types[n]

    def on_get_iter(self, path):
        #print "on_get_iter: %s %s " % (path, path[0])
        if len(self._keys) == 0:
            return None
        return path[0]

    def on_get_path(self, rowref):
        #print "on_get_path: %s" % rowref
        return (rowref,)

    def on_get_value(self, rowref, column):
        #print "on_get_value: %s " % rowref
        
        name = self._keys[rowref]
        pkg = self._cache[name]
        if column == LIST_ACTION:
            # order is (unfortunatelly) importend here
            if pkg.markedInstall:
                return self._icons[1]

            if pkg.markedUpgrade:
                return self._icons[0]
            if pkg.markedDelete:
                return self._icons[2]

            if pkg.isUpgradable:
                return self._icons[3]

            return None
        elif column == LIST_NAME:
            return pkg.name
        elif column == LIST_CONTENTS:
            s = "<big><b>%s</b></big>\n<small>%s</small>" % (pkg.name, pkg.summary)
            #s = pkg.name
            return s
        elif column == LIST_PKG:
            return pkg

    def on_iter_next(self, rowref):
        #print "on_iter_next"
        try:
            self._keys[rowref+1]
        except IndexError:
            return None
        else:
            return rowref+1

    def on_iter_children(self, parent):
        print "on_iter_children: %s " % parent
        #keys = self._cache.keys()
        if parent:
            return None
        return 0

    def on_iter_has_child(self, rowref):
        return False
    
    def on_iter_n_children(self, rowref):
        #print "on_iter_n_children: %s " % rowref
        #keys = self._cache.keys()
        if rowref:
            return 0
        return len(self._keys)

    def on_iter_nth_child(self, rowref, n):
        #print "on_iter_nth_child: %s %s" % (rowref, n)
        if rowref:
            return None
        try:
            self._keys[n]
        except IndexError:
            return None
        else:
            return n

    def on_iter_parent(self, child):
        #print "on_iter_parent"
        return None

    def check_for_changes(self):
        print "check_for_changed()"
        new = set(self._cache._filtered.keys())
        old = set(self._filtered_old.keys())
        if new == old:
            return
        added = new - old
        print "added: %s" % added
        removed = old - new
        print "removed: %s" % removed
        keys = self.update()

        # add/remove signals must be added in order
        # add
        add_list = [key for key in added]
        add_list.sort()
        for name in add_list:
            index = keys.index(name)
            print "added %s at index %s " % (name,index)
            path = (index,)
            iter = self.get_iter(path)
            print "%s %s" % (path, iter)
            self.row_inserted(path, iter)
        # remove
        remove_list = [key for key in removed]
        remove_list.sort()
        remove_list.reverse()
        for name in remove_list:
            keys = self._filtered_old.keys()
            for name in remove_list:
                index = keys.index(name)
                path = (index,)
                self.row_deleted(path)

    def snapshot(self):
        #print "snapshot()"
        self._filtered_old = copy.copy(self._cache._filtered)


    def cache_pre_change(self):
        #print "cache_pre_change"
        self.snapshot()

    def cache_post_change(self):
        #print "cache_post_changed"
        self.check_for_changes()

    def cache_pre_open(self):
        #print "cache_pre_open"
        # FIXME: add some code here to mark the cache as invalid
        # things will explode if we access the cache while it is
        # rebuilding
        self.snapshot()

    def cache_post_open(self):
        #print "cache_post_open, running update()"
        self.check_for_changes()





# -------------------------------------- testcode 
def clicked(treeview, path, view_column):
    #print "clicked: %s %s" % (path, view_column)
    model = treeview.get_model()
    #model.snapshot()
    pkg = model[path][2]
    if pkg.markedInstall or pkg.markedUpgrade:
        pkg.markKeep()
    else:
        pkg.markInstall()


    
class UpgradesFilter(apt.cache.Filter):
    def apply(self, pkg):
        if pkg.markedInstall or pkg.markedDelete or \
           pkg.markedUpgrade or pkg.isUpgradable:
            return True
        else:
            return False

class ChangesFilter(apt.cache.Filter):
    def apply(self, pkg):
        if pkg.markedInstall or pkg.markedDelete or \
           pkg.markedUpgrade:
            return True
        else:
            return False




if __name__ == "__main__":
    #cache = apt.Cache(apt.progress.OpTextProgress())
    cache = apt.cache.FilteredCache(progress=apt.progress.OpTextProgress())
    cache.Upgrade(True)
    #cache.SetFilter(ChangesFilter())
    cache.SetFilter(UpgradesFilter())

    win = gtk.Window()
    win.show()
    scroll = gtk.ScrolledWindow()
    scroll.show()
    treeview = gtk.TreeView()
    treeview.connect("row-activated", clicked)
    # renderer
    renderer = gtk.CellRendererPixbuf()
    column = gtk.TreeViewColumn("Pix", renderer, pixbuf=0)
    column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
    column.set_fixed_width(30)
    treeview.append_column(column)
    renderer = gtk.CellRendererText()
    column = gtk.TreeViewColumn("Apps", renderer, markup=1)
    column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
    column.set_fixed_width(300)
    treeview.append_column(column)
    treeview.set_fixed_height_mode(True)
    treeview.show()
    # build the model
    model = UpdateTreeModel(cache)
    
    scroll.add(treeview)
    win.add(scroll)
    win.set_usize(600,500)
    win.show()
    while gtk.events_pending():
        gtk.main_iteration()

    before = time.clock()
    treeview.set_model(model)
    while gtk.events_pending():
        gtk.main_iteration()
    after = time.clock()
    print "took: %s" % (after-before)

    time.sleep(1)
    cache["bison"].markKeep()
    cache["capplets"].markKeep()

    #for i in range(0,10):
    cache.Open(apt.progress.OpTextProgress())
    cache._reapplyFilter()
    treeview.set_model(UpdateTreeModel(cache))
    print len(cache.keys())
    while gtk.events_pending():
        gtk.main_iteration()
        
    
    gtk.main()
