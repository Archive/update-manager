#!/usr/bin/env python2.4

import apt

import pygtk; pygtk.require("2.0")
from gtk import gdk
import gtk
from gtk import gdk
import gobject
import time

class UpdateTreeModel(gtk.GenericTreeModel):
    column_types = (gtk.gdk.Pixbuf, str, gobject.TYPE_PYOBJECT)
    column_names = ['Pix', 'Name', 'Content']
    
    def __init__(self, cache, datadir=""):
        gtk.GenericTreeModel.__init__(self)
        self._cache=cache
        self._keys = cache.keys()
        self._keys.sort()
        self._icons = []
        p = "/usr/share/synaptic/pixmaps/package-"
        self._icons.append(gdk.pixbuf_new_from_file(p+"upgrade.png"))
        self._icons.append(gdk.pixbuf_new_from_file(p+"install.png"))
        self._icons.append(gdk.pixbuf_new_from_file(p+"remove.png"))
        self._icons.append(gdk.pixbuf_new_from_file(p+"installed-outdated.png"))

    # the treeview stuff
    def on_get_flags(self):
        #print "on_get_flags()"
        return gtk.TREE_MODEL_LIST_ONLY

    def on_get_n_columns(self):
        #print "on_get_n_columns()"
        return len(self.column_types)

    def on_get_column_type(self, n):
        #print "on_get_column_type()"
        return self.column_types[n]

    def on_get_iter(self, path):
        #print "on_get_iter: %s %s " % (path, path[0])
        #keys = self._cache.keys()
        return path[0]

    def on_get_path(self, rowref):
        print "on_get_path: %s" % rowref
        #keys = self._cache.keys()
        return (rowref,)

    def on_get_value(self, rowref, column):
        #print "on_get_value: %s " % rowref
        name = self._keys[rowref]
        pkg = self._cache[name]
        if column == 0:
            if pkg.markedUpgrade:
                return self._icons[0]
            elif pkg.markedInstall:
                return self._icons[1]
            elif pkg.markedDelete:
                return self._icons[2]
            elif pkg.isUpgradable:
                return self._icons[3]
            else:
                return None
        elif column == 1:
            #s = "<big><b>%s</b></big>\n<small>%s</small>" % (pkg.name, pkg.summary)
            s = pkg.name
            return s
        elif column == 2:
            return pkg

    def on_iter_next(self, rowref):
        try:
            self._keys[rowref+1]
        except IndexError:
            return None
        else:
            return rowref+1

    def on_iter_children(self, parent):
        print "on_iter_children: %s " % parent
        #keys = self._cache.keys()
        if parent:
            return None
        return 0

    def on_iter_has_child(self, rowref):
        return False
    
    def on_iter_n_children(self, rowref):
        #print "on_iter_n_children: %s " % rowref
        #keys = self._cache.keys()
        if rowref:
            return 0
        return len(self._keys)

    def on_iter_nth_child(self, rowref, n):
        #print "on_iter_nth_child: %s %s" % (rowref, n)
        #keys = self._cache.keys()
        if rowref:
            return None
        try:
            self._keys[n]
        except IndexError:
            return None
        else:
            return n

    def on_iter_parent(self, child):
        #print "on_iter_parent"
        return None


def clicked(treeview, path, view_column):
    print "clicked: %s %s" % (path, view_column)
    model = treeview.get_model()
    pkg = model[path][2]
    if pkg.markedInstall:
        pkg.markKeep()
    else:
        pkg.markInstall()
        
    # FIXME: performance sucks when the treemodel is big (or even medium)
    # all this SUCKS :/
    cache.reapplyFilter()
    #treeview.hide()
    #treeview.get_hadjustment().value_changed()
    #treeview.set_model(UpdateTreeModel(cache))
    #treeview.set_cursor(path)
    #treeview.scroll_to_cell(path)
    #treeview.show()
    
class UpgradesFilter(apt.cache.Filter):
    def apply(self, pkg):
        if pkg.markedInstall or pkg.markedDelete or \
           pkg.markedUpgrade or pkg.isUpgradable:
            return True
        else:
            return False
        


cache = None

if __name__ == "__main__":
    #cache = apt.Cache(apt.progress.OpTextProgress())
    cache = apt.cache.FilteredCache(apt.progress.OpTextProgress())
    cache.AddFilter(UpgradesFilter())

    cache.Upgrade()

    win = gtk.Window()
    win.show()
    scroll = gtk.ScrolledWindow()
    scroll.show()
    treeview = gtk.TreeView()
    treeview.connect("row-activated", clicked)
    # renderer
    renderer = gtk.CellRendererPixbuf()
    column = gtk.TreeViewColumn("Pix", renderer, pixbuf=0)
    column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
    column.set_fixed_width(30)
    treeview.append_column(column)
    renderer = gtk.CellRendererText()
    column = gtk.TreeViewColumn("Apps", renderer, markup=1)
    column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
    column.set_fixed_width(300)
    treeview.append_column(column)
    treeview.set_fixed_height_mode(True)
    treeview.show()
    # build the model
    model = UpdateTreeModel(cache)
    
    scroll.add(treeview)
    win.add(scroll)
    win.set_default_size(600,500)
    win.show()
    while gtk.events_pending():
        gtk.main_iteration()

    before = time.clock()
    treeview.set_model(model)
    while gtk.events_pending():
        gtk.main_iteration()
    after = time.clock()
    print "took: %s" % (after-before)
    print model.iter_n_children(None)

    gtk.main()
