# dialog_settings.py.in - edit some settings
#  
#  Copyright (c) 2005 Canonical
#  
#  Author: Michael Vogt <mvo@debian.org>
# 
#  This program is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU General Public License as 
#  published by the Free Software Foundation; either version 2 of the
#  License, or (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
#  USA

import gconf
import apt_pkg
import gtk
import os

periodicAptConfFile = "/etc/apt/apt.conf.d/10periodic"
archiveAptConfFile = "/etc/apt/apt.conf.d/20archive"

class dialog_settings:
  def save_periodic_config(self):
    #print "saving ..."

    # get the new values
    for key in self.conf_map:
        cb = self.gladexml.get_widget("checkbutton_%s"% key)
        sb = self.gladexml.get_widget("spinbutton_%s"% key)
        if cb and not cb.get_active():
            #print "%s=%s" % (self.conf_map[key], "0")
            apt_pkg.Config.Set(self.conf_map[key], "0")
        elif sb:
            value = sb.get_value()
            apt_pkg.Config.Set(self.conf_map[key], str(value))
            #print "%s=%s" % (self.conf_map[key], value)

    # special case for autodownload, it has the same interval as
    # Update-Package-Lists
    cb = self.gladexml.get_widget("checkbutton_autodownload")
    key = "autodownload"
    if cb.get_active():
        autoupdate = str(apt_pkg.Config.FindI("APT::Periodic::Update-Package-Lists"))
        apt_pkg.Config.Set(self.conf_map[key], autoupdate)
    else:
        apt_pkg.Config.Set(self.conf_map[key], "0")
        

    # write both config-prefixes to different files
    for (file, prefix) in ((periodicAptConfFile, "APT::Periodic"),
                           (archiveAptConfFile, "APT::Archives")):

        content = []
        if os.path.isfile(file):
            content=open(file,"r").readlines()

        cnf = apt_pkg.Config.SubTree(prefix)

        f = open(file,"w+")
        for line  in content:
            # don't write the udpated keys
            found = False
            for key in cnf.List():
                #print "%s=%s" % (key, cnf[key])
                if line.find("%s::%s" % (prefix,key)) >= 0:
                    found = True
                    break
            if not found:
                f.write(line)
        # write new keys
        for i in cnf.List():
            f.write("%s::%s \"%s\";\n" % (prefix,i,cnf.FindI((i))))
        f.close()

  def toggle_show_disabled(self, widget, data):
    self.show_disabled = widget.get_active()
    self.gconfclient.set_bool("/apps/gnome-software-properties/show_disabled",\
                              self.show_disabled)

  def toggle_show_version_details(self, widget, data):
    self.show_version_details = widget.get_active()
    self.gconfclient.set_bool("/apps/gnome-software-properties/show_version_details",\
                              self.show_version_details)

  def toggle_settings_cb(self, widget, data):
    mode = widget.get_active()
    self.gladexml.get_widget(data).set_sensitive(mode)

  def run(self):
    res = self.main_window.run()
    self.save_periodic_config()
    self.main_window.hide()
    return res
    
  def __init__(self, parent, glade):

    self.gladexml = glade
    self.main_window = self.gladexml.get_widget("dialog_settings")
    self.main_window.set_transient_for(parent)
    self.parent = parent
    self.gconfclient = gconf.client_get_default()
        
    # preferences entries
    self.show_disabled = self.gconfclient.get_bool("/apps/gnome-software-properties/show_disabled")
    self.show_version_details = self.gconfclient.get_bool("/apps/gnome-software-properties/show_version_details")

    checkbutton_show_disabled = self.gladexml.get_widget("checkbutton_show_disabled")
    checkbutton_show_disabled.set_active(self.show_disabled)
    checkbutton_show_disabled.connect("toggled", self.toggle_show_disabled, None)

    checkbutton_show_version_details = self.gladexml.get_widget("checkbutton_show_version_details")
    checkbutton_show_version_details.set_active(self.show_version_details)
    checkbutton_show_version_details.connect("toggled", self.toggle_show_version_details, None)

    # apt-config
    
    # set the update stuff
    self.conf_map = {
        "autoupdate"   : "APT::Periodic::Update-Package-Lists",
        "autodownload" : "APT::Periodic::Download-Upgradeable-Packages",
        "autoclean"    : "APT::Periodic::AutocleanInterval",
        "max_size"     : "APT::Archives::MaxSize",
        "max_age"      : "APT::Archives::MaxAge"
        }

    for key in self.conf_map:
        value = apt_pkg.Config.FindI(self.conf_map[key])
        #print "%s=%s" % (key, value)
        cb = self.gladexml.get_widget("checkbutton_%s"% key)
        #if cb == None:
        #    print "checkbutton_%s not found" % key
        sb = self.gladexml.get_widget("spinbutton_%s"% key)
        if sb != None:
            #print "setting %s to %s" % (key, value)
            sb.set_value(value)
        #else:
        #    print "spinbutton_%s not found" % key
        box = self.gladexml.get_widget("vbox_%s"% key)
        #if box == None:
        #    print "vbox_%s not found" % key
        if box and cb:
            cb.connect("toggled", self.toggle_settings_cb, ("vbox_%s" % key))
        if cb:
            cb.set_active(value)
         
